import express, { Application, Request, Response, NextFunction } from 'express';
import cors from 'cors';
import bodyparser from 'body-parser';
import mongoose from 'mongoose';

import CarRouter from './api/CarRouter';
import CustomerRouter from './api/CustomerRouter';
import ContractRouter from './api/ContractRouter';
import PaymentRouter from './api/PaymentRouter';
import SparePartRouter from './api/SparePartRouter';

let app: Application = express();

if (app.get('env') == 'development') {
  require('dotenv').config();
}

 
const PORT: Number = parseInt(process.env.PORT as string, 10) || 5000;
const CONNECT_STRING: string = (process.env.CONNECT_STRING as string) || 'mongodb://user1234:user1234@ds159845.mlab.com:59845/klimekp-shop-production';

//Connect to
mongoose.connect(CONNECT_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

//mongoose.Promise = global.Promise;

//error midleare

app.use('/api/car', cors(), bodyparser.json(), CarRouter);
app.use('/api/customer', cors(), bodyparser.json(), CustomerRouter);
app.use('/api/contract', cors(), bodyparser.json(), ContractRouter);
app.use('/api/payment', cors(), bodyparser.json(), PaymentRouter);
app.use('/api/sparepart', cors(), bodyparser.json(), SparePartRouter);

app.use(bodyparser.json());

app.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.sendStatus(200);
});

//Error handler midlleware
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  //tady odtud mi to jede dobre
  //console.log(err.message)
  res.status(409).send({ error: err.message });
});

// error handler middleware

app.listen(PORT, () => console.log(`Your port is ${PORT}`));
