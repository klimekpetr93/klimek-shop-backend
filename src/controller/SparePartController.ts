import { Request, Response, NextFunction } from 'express';

import SparePart from '../model/SparePart';
import Contract from '../model/Contract';
import Customer from '../model/Customer';
import Payment from '../model/Payment';
import Car from '../model/Car';
import { IContract, ISparePart } from '../model/interfaces/Interfaces';

export let AllSparePart = (req: Request, res: Response, next: NextFunction) => {
  SparePart.find({}, (err: any, SpareParts: any) => {
    if (SpareParts) {
      res.json(SpareParts);
    }
  }).catch(next);
};

export let OneSparePart = (req: Request, res: Response, next: NextFunction) => {
  SparePart.findOne({ _id: req.params.id }, (err: any, sparePart: any) => {
    if (sparePart) {
      res.json(sparePart);
    }
  }).catch(next);
};

export let AddSparePartToContract = (req: Request, res: Response, next: NextFunction) => {
  SparePart.create(req.body, (err: any, sparePart: any) => {
    Contract.findByIdAndUpdate({ _id: req.params.contract_id }, { $push: { spareParts: sparePart._id } }, { new: true }, (err: any, contract: any) => {
      contract.save(() => res.send(contract));
    }).populate('spareParts');
  });
};

export let DeleteSparePart = (req: Request, res: Response, next: NextFunction) => {
  SparePart.findByIdAndRemove({ _id: req.params.id }, req.body)
    .then(() => {
      Contract.findOneAndUpdate({ _id: req.body.contract_id }, { $pull: { spareParts: req.params.id } }, (err: any, contract: any) => {
        contract.save(() => res.send(contract));
      }).populate('spareParts');
    })
    .catch(next);
};

export let UpdateSparePart = (req: Request, res: Response, next: NextFunction) => {
  SparePart.updateOne({ _id: req.params.id }, req.body)
    .then(() => {
      Contract.findById({ _id: req.body.contract_id }, (err: any, contract: any) => {
        contract.save(() => res.send(contract));
      }).populate('spareParts');
    })
    .catch(next);
};

export let DeleteAllPart = (req: Request, res: Response, next: NextFunction) => {
  SparePart.deleteMany({})
    .then(() => Contract.deleteMany({}))
    .then(() => Customer.deleteMany({}))
    .then(() => Payment.deleteMany({}))
    .then(() => Car.deleteMany({}))
    .then(() => res.sendStatus(200));
};
