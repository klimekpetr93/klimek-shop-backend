import Customer from '../model/Customer';

import { Request, Response, NextFunction } from 'express';

export let AllCustomer = (req: Request, res: Response, next: NextFunction) => {
  Customer.find({}, (err: any, Customers: any) => {
    if (Customers) {
      res.json(Customers);
    }
  }).catch(next);
};

export let OneCustomer = (req: Request, res: Response, next: NextFunction) => {
  Customer.findOne({ _id: req.params.id }, (err: any, customer: any) => {
    if (customer) {
      res.json(customer);
    }
  }).catch(next);
};

export let AddCustomer = (req: Request, res: Response, next: NextFunction) => {
  Customer.create(req.body)
    .then(createdCustomer => res.send(createdCustomer))
    .catch(next); //na serveru.ts se zavola Error handler midlleware a ten posle error co se stalo na klienta...
};

export let DeleteCustomer = (req: Request, res: Response, next: NextFunction) => {
  Customer.findByIdAndRemove({ _id: req.params.id })
    .then(() => res.sendStatus(200))
    .catch(next);
};

export let UpdateCustomer = (req: Request, res: Response, next: NextFunction) => {
  Customer.updateOne({ _id: req.params.id }, req.body)
    .then(updatedCustomer => res.send(updatedCustomer))
    .catch(next);
};

//send back customer by ID with all cars
export let OneCustomerWithCars = (req: Request, res: Response, next: NextFunction) => {
  Customer.findOne({ _id: req.params.id }, (err: any, Customers: any) => {
    if (Customers) {
      res.json(Customers);
    }
  }).populate('cars')
    .catch(next);
};

//send back customer by ID with all payments
export let OneCustomerWithPayments = (req: Request, res: Response, next: NextFunction) => {
  Customer.findOne({ _id: req.params.id }, (err: any, Customers: any) => {
    if (Customers) {
      res.json(Customers);
    }
  })
    .populate('payments')
    .catch(next);
};

//send back customer by ID with all contract
export let OneCustomerWithContract = (req: Request, res: Response, next: NextFunction) => {
  Customer.findOne({ _id: req.params.id }, (err: any, Customers: any) => {
    if (Customers) {
      res.json(Customers);
    }
  })
    .populate('contracts')
    .catch(next);
};

//send back customer by ID with all contract
export let OneCustomerWithAll = (req: Request, res: Response, next: NextFunction) => {
  Customer.findOne({ _id: req.params.id }, (err: any, Customers: any) => {
    if (Customers) {
      res.json(Customers);
    }
  })
    .populate('cars')
    .populate('payments')
    .populate('contracts')
    .catch(next);
};
