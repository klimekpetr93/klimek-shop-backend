import Car from '../model/Car';
import Customer from '../model/Customer';

import { Request, Response, NextFunction } from 'express';
import { ICar } from '../model/interfaces/Interfaces';

export let AllCar = (req: Request, res: Response, next: NextFunction) => {
  Car.find({}, (err: any, Cars: any) => {
    if (Cars) {
      res.json(Cars);
    }
  }).catch(next);
};

export let OneCar = (req: Request, res: Response, next: NextFunction) => {
  Car.findOne({ _id: req.params.id }, (err: any, Car: ICar) => {
    if (Car) {
      res.json(Car);
    }
  }).catch(next);
};

export let AddCar = (req: Request, res: Response, next: NextFunction) => {
  Car.create(req.body)
    .then(createdCar =>
      Customer.findOneAndUpdate({ _id: req.params.customer_id }, { $push: { cars: createdCar._id } }, { new: true })
        .populate('cars')
        .then(updatedCustomer => res.send(updatedCustomer)),
    )
    .catch(next); //na serveru.ts se zavola Error handler midlleware a ten posle error co se stalo na klienta...
};

export let DeleteCar = (req: Request, res: Response, next: NextFunction) => {
  Car.findOne({ _id: req.params.id }, (err: any, finded: ICar) => {
    if (err) next(err);
    else {
      finded.remove((err: any, car: ICar) => {
        if (err) next(err);
        else {
          Customer.findOneAndUpdate({ _id: req.body.customer_id }, { $pull: { cars: req.params.id } }, (err: any, customer: any) => {
            if (err) next(err);
            //  else customer.save(customer  => res.send(customer));
          })
            .populate('cars')
            .then(updatedCustomer => res.send(updatedCustomer));
        }
      });
    }
  });
};

export let UpdateCar = (req: Request, res: Response, next: NextFunction) => {
  Car.updateOne({ _id: req.params.id }, req.body)
    .then(() =>
      Customer.findOne({ _id: req.body.customer_id })
        .populate('cars')
        .then(updatedCustomer => res.send(updatedCustomer)),
    )
    .catch(next);
};
