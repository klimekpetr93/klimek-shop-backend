import Payment from '../model/Payment';

import { Request, Response, NextFunction } from 'express';
import Customer from '../model/Customer';
import { IPayment, ICustomer } from '../model/interfaces/Interfaces';

export let AllPayment = (req: Request, res: Response, next: NextFunction) => {
  Payment.find({}, (err: any, Payments: any) => {
    if (Payments) {
      res.json({ Payments: 'cay' });
    }
  }).catch(next);
};

export let OnePayment = (req: Request, res: Response, next: NextFunction) => {
  Payment.findOne({ _id: req.params.id }, (err: any, payment: any) => {
    if (payment) {
      res.json(payment);
    }
  }).catch(next);
};

export let AddPayment = (req: Request, res: Response, next: NextFunction) => {
  Payment.create(req.body, (err: any, payment: any) => {
    Customer.findById({ _id: req.params.customer_id }, (err: any, customer: ICustomer) => {
      customer.payments.push(payment);
      customer.save(() => res.send(customer));
    })
      .populate('contracts')
      .populate('payments')
      .populate('cars');
  });
};

export let DeletePayment = (req: Request, res: Response, next: NextFunction) => {
  Payment.deleteOne({ _id: req.params.id }, req.body, () => {
    Customer.findById({ _id: req.body.customer_id }, (err: any, customer: ICustomer) => {
      customer.payments.pull(req.body);
      customer.save(() => res.send(customer));
    })
      .populate('contracts')
      .populate('payments')
      .populate('cars');
  });
};

export let UpdatePayment = (req: Request, res: Response, next: NextFunction) => {
  Payment.updateOne({ _id: req.params.id }, req.body, (err: any, payment: IPayment) => {
    Customer.findById({ _id: req.body.customer_id }, (err: any, customer: ICustomer) => {
      customer.save(() => res.send(customer));
    })
      .populate('contracts')
      .populate('payments')
      .populate('cars');
  });
};
