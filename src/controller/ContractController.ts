import Contract from '../model/Contract';
import Customer from '../model/Customer';

import { Request, Response, NextFunction } from 'express';
import { ICustomer, IContract } from '../model/interfaces/Interfaces';

export let AllContract = (req: Request, res: Response, next: NextFunction) => {
  Contract.find({}, (err: any, Contracts: any) => {
    if (Contracts) {
      res.json(Contracts);
    }
  }).catch(next);
};

export let OneContract = (req: Request, res: Response, next: NextFunction) => {
  Contract.findOne({ _id: req.params.id }, (err: any, Contracts: any) => {
    if (Contracts) {
      res.json(Contracts);
    }
  })
    .populate('spareParts')
    .catch(next);
};

export let AddContract = (req: Request, res: Response, next: NextFunction) => {
  Contract.create(req.body, (err: any, contract: any) => {
    Customer.findById({ _id: req.params.customer_id }, (err: any, customer: ICustomer) => {
      customer.contracts.push(contract);
      customer.save(() => res.send(customer));
    })
      .populate('contracts')
      .populate('payments')
      .populate('cars');
  });
};

export let DeleteContract = (req: Request, res: Response, next: NextFunction) => {
  Contract.deleteOne({ _id: req.params.id }, req.body, () => {
    Customer.findById({ _id: req.body.customer_id }, (err: any, customer: ICustomer) => {
      customer.contracts.pull(req.body);
      customer.save(() => res.send(customer));
    })
      .populate('contracts')
      .populate('payments')
      .populate('cars');
  });
};

export let UpdateContract = (req: Request, res: Response, next: NextFunction) => {
  Contract.update({ _id: req.params.id }, req.body, (err: any, contract: IContract) => {
    Contract.findById({ _id: req.params.id }, (err: any, contract: IContract) => {
      contract.save(() => res.send(contract));
    })
      .populate('spareParts')
      .populate('payments')
      .populate('cars');
  });
};
