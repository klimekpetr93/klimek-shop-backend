import express, { Router } from 'express';
//COntroller import
import * as CustomerController from '../controller/CustomerController';

let customerRoutes: Router = express.Router();

customerRoutes.get('/', CustomerController.AllCustomer);

customerRoutes.get('/:id', CustomerController.OneCustomer);

customerRoutes.post('/', CustomerController.AddCustomer);

customerRoutes.delete('/:id', CustomerController.DeleteCustomer);

customerRoutes.patch('/:id', CustomerController.UpdateCustomer);

customerRoutes.get('/cars/:id', CustomerController.OneCustomerWithCars);

customerRoutes.get('/payments/:id', CustomerController.OneCustomerWithPayments);

customerRoutes.get('/contracts/:id', CustomerController.OneCustomerWithContract);

customerRoutes.get('/all/:id', CustomerController.OneCustomerWithAll);

export default customerRoutes;
