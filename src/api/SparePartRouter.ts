import express, { Router } from 'express';
//COntroller import
import * as SparePartController from '../controller/SparePartController';

let sparePartRoutes: Router = express.Router();

sparePartRoutes.get('/', SparePartController.AllSparePart);

sparePartRoutes.get('/:id', SparePartController.OneSparePart);

//delete spare parts by id, and  by customer_id in req.body pull from spareParts on contract, and count totalCost, income and profit
sparePartRoutes.post('/:contract_id', SparePartController.AddSparePartToContract);

sparePartRoutes.patch('/:id', SparePartController.UpdateSparePart);

sparePartRoutes.delete('/:id', SparePartController.DeleteSparePart);

sparePartRoutes.delete('/all/allWithContracts', SparePartController.DeleteAllPart);

export default sparePartRoutes;
