import express, { Router } from 'express';
//COntroller import
import * as ContractController from '../controller/ContractController';

let contractRoutes: Router = express.Router();
contractRoutes.get('/', ContractController.AllContract);

contractRoutes.get('/:id', ContractController.OneContract);

contractRoutes.post('/:customer_id', ContractController.AddContract);

contractRoutes.delete('/:id', ContractController.DeleteContract);

contractRoutes.patch('/:id', ContractController.UpdateContract);


export default contractRoutes;
