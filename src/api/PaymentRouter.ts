import express, { Router } from 'express';
//COntroller import
import * as PaymentController from '../controller/PaymentController';

let paymentRoutes: Router = express.Router();

paymentRoutes.get('/', PaymentController.AllPayment);

paymentRoutes.get('/:id', PaymentController.OnePayment);

paymentRoutes.post('/:customer_id', PaymentController.AddPayment);

paymentRoutes.delete('/:id', PaymentController.DeletePayment);

paymentRoutes.patch('/:id', PaymentController.UpdatePayment);

//paymentRoutes.post('/customer/:id', PaymentController.CreatePaymentToCustomer);

export default paymentRoutes;
