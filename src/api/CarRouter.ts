import express, { Router } from 'express';
//COntroller import
import * as CarController from '../controller/CarController';

let carRoutes: Router = express.Router();
//api/car
carRoutes.get('/', CarController.AllCar);
//api/car/:id
carRoutes.get('/:id', CarController.OneCar);
//api/car/:customer_id
//create car and update customer
carRoutes.post('/:customer_id', CarController.AddCar);

carRoutes.patch('/:id', CarController.UpdateCar);
 
//api/car/delete/:id
carRoutes.delete('/:id', CarController.DeleteCar);
 

export default carRoutes;
