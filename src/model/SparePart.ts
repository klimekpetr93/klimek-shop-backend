import mongoose, { Schema } from 'mongoose';
import { ISparePart } from './interfaces/Interfaces';

let SparePartSchema: Schema = new Schema({
  numberCode: String,
  name: String,
  producer: String, // manufactured by?
  suplier: String, //dodavatel
  count: Number, 
  purchasePrice: Number, //za kolik kupuju
  sellingPrice: Number, // za kolik prodavam
  profit: Number, // (sellingPrice - purchasePrice)  
  totalPurchasePrice: Number, //purchasePrice * count
  totalSellingPrice: Number, // sellingPrice * count
  totalProfit: Number, // (sellingPrice - purchasePrice) * count
  contract_id: {
    type: Schema.Types.ObjectId,
    ref: 'Contract',
  },
});

// slo by napsat tak ze tu definici bych dal primo sem, ale budu FIlmSChema dal pak pouzivat treba na virtual atd ...
export default mongoose.model<ISparePart>('SparePart', SparePartSchema);
