import mongoose, { Schema } from 'mongoose';
import { ICustomer, IContract } from './interfaces/Interfaces';

let CustomerSchema: Schema = new Schema({
  name: { type: String, required: [true, 'Name is required'] },
  surname: { type: String, required: [true, 'Surname is required'] },
  city: { type: String },
  street: { type: String },
  email: { type: String },
  phone: { type: String },
  postNumber: { type: String },
  contracts: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Contract',
    },
  ],
  payments: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Payment',
    },
  ],
  cars: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Car',
    },
  ],
  totalIncomeWork: Number, //sum of incomeWork from contracts,
  totalCostsParts: Number, //sum of costParts from contracts,
  totalIncomeParts: Number, //sum of incomeParts from contracts,
  totalProfitParts: Number, //sum of totalProfitParts from contracts
  totalProfit: Number, //sum of totalProfitParts from contracts
  totalPayment: Number, //(sum of amount on payments)
  debt: Number, //cisty zisk od zakaznika  totalProfitPartsWork -totalPayment
});

CustomerSchema.pre<ICustomer>('save', function (next: any) {
  this.totalIncomeWork = this.contracts.reduce((r: any, d: any) => r + d.incomeWork, 0);
  this.totalCostsParts = this.contracts.reduce((r: any, d: any) => r + d.costParts, 0);
  this.totalIncomeParts = this.contracts.reduce((r: any, d: any) => r + d.incomeParts, 0);
  this.totalProfitParts = this.contracts.reduce((r: any, d: any) => r + d.totalProfitParts, 0);
  this.totalProfit = Number(this.totalIncomeParts) + Number(this.totalIncomeWork);
  this.totalPayment = this.payments.reduce((r: any, d: any) => r + d.amount, 0);
  this.debt = Number(this.totalProfit) - Number(this.totalPayment);
  next();
});

// slo by napsat tak ze tu definici bych dal primo sem, ale budu FIlmSChema dal pak pouzivat treba na virtual atd ...
export default mongoose.model<ICustomer>('Customer', CustomerSchema);
