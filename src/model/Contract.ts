import mongoose, { Schema } from 'mongoose';
import { IContract } from './interfaces/Interfaces';
import Customer from './Customer';

let ContractSchema: Schema = new Schema({
  startDate: Date,
  endDate: Date,
  hourCount: Number,
  hourRate: Number,
  incomeWork: Number, // hourCount * hourRate
  costParts: Number, // sum of totalPurchasePrice from spareParts
  incomeParts: Number, //sum of totalSellingPrice from spareParts
  totalProfitParts: Number, // (sum totalProfit from spare parts )
  totalProfit: Number, // (sum totalProfit from spare parts )+ incomeOnWork
  customer_id: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
  },
  car_id: {
    type: Schema.Types.ObjectId,
    ref: 'Car',
  },
  spareParts: [
    {
      type: Schema.Types.ObjectId,
      ref: 'SparePart',
    },
  ],
  completed: Boolean,
  name: String,
  notes: String,
});

// slo by napsat tak ze tu definici bych dal primo sem, ale budu FIlmSChema dal pak pouzivat treba na virtual atd ...

ContractSchema.pre<IContract>('save', function (next: any) {
  console.log('Save start. 1');
  this.costParts = this.spareParts.reduce((r: any, d: any) => r + d.totalPurchasePrice, 0);
  this.incomeParts = this.spareParts.reduce((r: any, d: any) => r + d.totalSellingPrice, 0);
  this.totalProfitParts = this.spareParts.reduce((r: any, d: any) => r + d.totalProfit, 0);
  this.totalProfit = Number(this.totalProfitParts) + Number(this.incomeWork);
  next();
});

ContractSchema.post<IContract>('save', function (contract: IContract, next: any) {
  console.log('After save.');
  Customer.findById({ _id: contract.customer_id }, (err: any, customer) => {
    customer?.save({});
  })
    .populate('contracts')
    .populate('payments');
  next();
});
export default mongoose.model<IContract>('Contract', ContractSchema);
