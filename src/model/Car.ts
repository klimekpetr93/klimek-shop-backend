import mongoose, { Schema } from 'mongoose';
import Contract from './Contract';

import { ICar } from './interfaces/Interfaces';

let CarSchema: Schema = new Schema({
  manufacturedYear: Number,
  spz: String,
  factory: String,
  winCode: String,
  customer_id: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
  },
});


CarSchema.pre('remove', function(next: any) {
  Contract.findOne({ car_id: this._id }, (err: any, result: any) => {
    if (result) {
      let error = new Error('Nasel jsem zakazku k autu');
      next(error);
    } else {
      next();
    }
  });
});

export default mongoose.model<ICar>('Car', CarSchema);
