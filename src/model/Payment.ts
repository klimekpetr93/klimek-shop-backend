import mongoose, { Schema } from 'mongoose';
import { IPayment } from './interfaces/Interfaces';
import Customer from './Customer';

let PaymentSchema: Schema = new Schema({
  date: { type: Date, required: [true, 'Date is required'] },
  amount: { type: Number, required: [true, 'Amount is required'] },
  customer_id: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
  },
  contracts: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Contract',
    },
  ],
});

PaymentSchema.post<IPayment>('save', function(payment: IPayment, next: any) {
  Customer.findById({ _id: payment.customer_id }, (err: any, customer) => {
    customer?.save({});
  }).populate('payments').populate('contracts');
  next();
});

// slo by napsat tak ze tu definici bych dal primo sem, ale budu FIlmSChema dal pak pouzivat treba na virtual atd ...
export default mongoose.model<IPayment>('Payment', PaymentSchema);
