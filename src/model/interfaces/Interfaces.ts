import { Document, Types } from 'mongoose';

export interface ICar extends Document {
  manufacturedYear: Number;
  spz: String;
  factory: String;
  winCode: String;
  customer_id: Document['_id'];
}

export interface ICustomer extends Document {
  name: String;
  surname: String;
  city: String;
  street: String;
  email: String;
  phone: String;
  postNumber: String;
  contracts: Types.Array<Document['_id']>;
  payments: Types.Array<Document['_id']>;
  cars: Types.Array<Document['_id']>;
  totalIncomeWork: Number; //sum of incomeWork from contracts,
  totalCostsParts: Number; //sum of costParts from contracts,
  totalIncomeParts: Number; //sum of incomeParts from contracts,
  totalProfitParts: Number; //sum of totalProfitParts from contracts
  totalProfit: Number; //sum of totalProfitParts from contracts
  totalPayment: Number; //(sum of amount on payments)
  debt: Number; //cisty zisk od zakaznika  totalProfitPartsWork -totalPayment
}

export interface IContract extends Document {
  name: String;
  startDate: Date;
  endDate: Date;
  hourCount: Number;
  hourRate: Number;
  incomeWork: Number; // hourCount * hourRate
  costParts: Number; // sum of totalPurchasePrice from spareParts
  incomeParts: Number; //sum of totalSellingPrice from spareParts
  totalProfitParts: Number; // (sum totalProfit from spare parts )
  totalProfit: Number; // (sum totalProfit from spare parts )+ incomeOnWork
  customer_id: Document['_id'];
  car_id: Document['_id'];
  spareParts: Types.Array<Document['_id']>;
  completed: Boolean;
  notes: String;
}

export interface IPayment extends Document {
  date: Date;
  amount: Number;
  customer_id: Document['_id'];
  contracts: Types.Array<Document['_id']>;
}

export interface ISparePart extends Document {
  numberCode: String;
  name: String;
  producer: String; // manufactured by?
  suplier: String; //dodavatel
  count: Number;
  purchasePrice: Number; //za kolik kupuju
  sellingPrice: Number; // za kolik prodavam
  profit: Number; //kolik sem vydelal
  totalPurchasePrice: Number; //purchasePrice * count
  totalSellingPrice: Number; // sellingPrice * count
  totalProfit: Number; // (sellingPrice - purchasePrice) * count
  contract_id: Document['_id'];
}
